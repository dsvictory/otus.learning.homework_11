import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Paper from "@mui/material/Paper";
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useState } from "react";
import axios from "axios";

export default function LoginForm()  {

    const [isSuccess, setIsSuccess] = useState(false);
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");

    const loginChangedhandler = (event) => {
        const value = event.target.value;
        setLogin(value);
    };

    const passwordChangedhandler = (event) => {
        const value = event.target.value;
        setPassword(value);
    };

    const submitHandler = (event) => {
        event.preventDefault();
        
        const data = {
            login,
            password
        };
        // http://localhost:5000
        axios.post(`/api/login`, data).then((response) => {
                console.log(response.data);
                setIsSuccess(true);
            });
    };

    const message = isSuccess ? "You're logged in!" : "Let's login, tutor!";

    return (
        <form onSubmit={submitHandler}>
            <Box
                sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
                height: "100%",
                position: "absolute"
            }}
            >
                <Paper elevation='3' sx={{
                    display: "inline-flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    p: 1,
                    m: 1,
                    borderRadius: 1,
                    fontSize: '1.5rem',
                    "& .MuiTextField-root": { m: 1, width: '25ch' },
                }}>
                    <Typography sx={{ textAlign: "center", fontWeight: "bold" }} variant="h4" gutterBottom component="div">
                        {message}
                    </Typography>
                    <TextField
                        //required
                        id="loggin-field"
                        label="Login"
                        onChange={loginChangedhandler}
                        value={login}
                    />
                    <TextField
                        id="password-field"
                        label="Password"
                        type="password"
                        onChange={passwordChangedhandler}
                        value={password}
                    />
                    <Button type="submit" variant="contained" color={isSuccess ? "success" : "primary"}>Try</Button>
                </Paper>
            </Box>
        </form>
    );
};