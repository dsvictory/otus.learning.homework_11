const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser')

const app = express();
app.use(bodyParser.json());
app.use(cors());

const port = 5000;

app.post('/api/login', (req, res) => {
    console.log(req.body);
    res.send("That's right!");
});

app.listen(port, () => console.log(`Miniserver listening on port ${port}!`))